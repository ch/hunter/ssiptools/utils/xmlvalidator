.. xmlvalidator documentation master file, created by
   sphinx-quickstart on Tue Dec 31 19:17:42 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to xmlvalidator's documentation!
========================================
.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README.md
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
