#    xmlvalidator reads and validates XML files against XML schema.
#    Copyright (C) 2019  Mark D. Driver
#
#    xmlvalidator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
from setuptools import setup

setup(name='xmlvalidator',
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      description='python scripts for reading and validating XML against XML Schema using lxml.',
      url='https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/utils/xmlvalidator',
      author='Mark Driver',
      author_email='mdd31@cantab.ac.uk',
      license='AGPLv3',
      packages=setuptools.find_packages(),
      package_data={'':['test/resources/ethanolnamespaced.cml']},
      install_requires=['lxml',],
      zip_safe=False)
