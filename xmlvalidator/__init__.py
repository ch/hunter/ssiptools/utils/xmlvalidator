# -*- coding: utf-8 -*-
#    xmlvalidator reads and validates XML files against XML schema.
#    Copyright (C) 2019  Mark D. Driver
#
#    xmlvalidator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Init contains top level methods/variables commonly required for convenience.

Import Schema and method for validation and reading of a file. Path to example
file also included.

:Authors:
    Mark Driver <mdd31>

Attributes
----------
CML_SCHEMA_PATH : str
    HTTP address of fully namespace qualified CML schema

SSIP_SCHEMA_PATH : str
    HTTP address of SSIP XML Schema

PHASE_SCHEMA_PATH : str
    HTTP address of Phase XML Schema

HUNTER_DB_SCHEMA_PATH : str
    HTTP address of Hunter database XML Schema

PHASE_CALCULATOR_SCHEMA_PATH : str
    HTTP address of Phase calculator XML Schema

CML_SCHEMA : lxml.etree.XMLSchema
    Schema instance of fully namespace qualified CML schema

SSIP_SCHEMA : lxml.etree.XMLSchema
    Schema instance of SSIP XML Schema

PHASE_SCHEMA : lxml.etree.XMLSchema
    Schema instance of  Phase XML Schema

HUNTER_DB_SCHEMA : lxml.etree.XMLSchema
    Schema instance of Hunter database XML Schema

PHASE_CALCULATOR_SCHEMA : lxml.etree.XMLSchema
    Schema instance of Phase calculator XML Schema

CML_ETHANOL_EXAMPLE : str
    URI path for example CML file for validation.
    
validate_and_read_xml_file : func
    Function from xmlvalidation
    
See Also
--------

xmlvalidator.xmlvalidation.validate_and_read_xml_file : XML validation function.

"""
import pathlib
from xmlvalidator.xmlvalidation import (
    CML_SCHEMA,
    SSIP_SCHEMA,
    PHASE_SCHEMA,
    HUNTER_DB_SCHEMA,
    PHASE_CALCULATOR_SCHEMA,
    validate_and_read_xml_file,
)

CML_ETHANOL_EXAMPLE = (
    (pathlib.Path(__file__).parents[0] / "test" / "resources" / "ethanolnamespaced.cml")
    .absolute()
    .as_uri()
)
