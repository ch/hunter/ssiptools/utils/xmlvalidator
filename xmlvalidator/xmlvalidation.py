# -*- coding: utf-8 -*-
#    xmlvalidator reads and validates XML files against XML schema.
#    Copyright (C) 2019  Mark D. Driver
#
#    xmlvalidator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script containing methods for reading in XML files to etree representation,
and also methods for Schema validation.



:Authors:
    Mark Driver <mdd31>

Attributes
----------
CML_SCHEMA_PATH : str
    HTTP address of fully namespace qualified CML schema

SSIP_SCHEMA_PATH : str
    HTTP address of SSIP XML Schema

PHASE_SCHEMA_PATH : str
    HTTP address of Phase XML Schema

HUNTER_DB_SCHEMA_PATH : str
    HTTP address of Hunter database XML Schema

PHASE_CALCULATOR_SCHEMA_PATH : str
    HTTP address of Phase calculator XML Schema

CML_SCHEMA : lxml.etree.XMLSchema
    Schema instance of fully namespace qualified CML schema

SSIP_SCHEMA : lxml.etree.XMLSchema
    Schema instance of SSIP XML Schema

PHASE_SCHEMA : lxml.etree.XMLSchema
    Schema instance of  Phase XML Schema

HUNTER_DB_SCHEMA : lxml.etree.XMLSchema
    Schema instance of Hunter database XML Schema

PHASE_CALCULATOR_SCHEMA : lxml.etree.XMLSchema
    Schema instance of Phase calculator XML Schema

"""
import logging
from lxml import etree

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)

CML_SCHEMA_PATH = "http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd"

SSIP_SCHEMA_PATH = "http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd"

PHASE_SCHEMA_PATH = "http://www-hunter.ch.cam.ac.uk/schema/PhaseSchema.xsd"

HUNTER_DB_SCHEMA_PATH = "http://www-hunter.ch.cam.ac.uk/schema/HunterDatabaseSchema.xsd"

PHASE_CALCULATOR_SCHEMA_PATH = (
    "http://www-hunter.ch.cam.ac.uk/schema/PhaseCalculatorSchema.xsd"
)


def validate_and_read_xml_file(xml_filename, schema_filename):
    """Read in a file and checks that it is valid.

    Parameters
    ----------
    xml_filename: str
    schema_filename: str or lxml.etree.XMLSchema
        Schema to be used in validation, either a file path to be read or a Schema object.

    Returns
    -------
    xml_etree: lxml.etree.Etree
        If valid XML file is read in etree is returned.

    Raises
    ------
    lxml.etree.DocumentInvalid
        If the document is not valid.
    """
    xml_etree = read_xml_file(xml_filename)
    valid = validate_xml(xml_etree, schema_filename)
    LOGGER.info("validation: ")
    LOGGER.info(valid)
    return xml_etree


def validate_xml(xml_etree, schema_file):
    """Validate the xml_etree using the schema.

    Parameters
    ----------
    xml_etree: lxml.Etree
        XML etree to be validated.
    schema_file: str or lxml.etree.XMLSchema
        Schema to be used in validation, either a file path to be read or a Schema object.

    Raises
    ------
    lxml.etree.DocumentInvalid
        If the document is not valid.
    """
    if isinstance(schema_file, etree.XMLSchema):
        schema = schema_file
    else:
        schema = create_xml_schema(schema_file)
    return validate_xml_file(xml_etree, schema)


def validate_xml_file(file_etree, schema):
    """Check to see if the file conforms to the given schema.

    Parameters
    ----------
    file_etree: lxml.etree.Etree
    schema: lxml.etree.XMLSchema

    Raises
    ------
    lxml.etree.DocumentInvalid
        If the document is not valid.
    """
    return schema.assertValid(file_etree)


def create_xml_schema(filename):
    """Read in file to etree representation, then tries to create schema.

    Parameters
    ----------
    filename: str
        filename of schema

    Returns
    -------
    xml_schema: lxml.etree.XMLSchema
        XMLSchema instance
    """
    xml_file = read_xml_file(filename)
    return etree.XMLSchema(xml_file)


def read_xml_file(filename):
    """Read in xml file to ElementTree representation.

    Parameters
    ----------
    filename: str
        file to be read.

    Returns
    -------
    xml_file: lxml.etree.Etree
        Etree representation of file
    """
    LOGGER.debug("Reading in file: %s", filename)
    xml_file = etree.parse(filename)
    return xml_file


CML_SCHEMA = create_xml_schema(CML_SCHEMA_PATH)

SSIP_SCHEMA = create_xml_schema(SSIP_SCHEMA_PATH)

PHASE_SCHEMA = create_xml_schema(PHASE_SCHEMA_PATH)

HUNTER_DB_SCHEMA = create_xml_schema(HUNTER_DB_SCHEMA_PATH)

PHASE_CALCULATOR_SCHEMA = create_xml_schema(PHASE_CALCULATOR_SCHEMA_PATH)
