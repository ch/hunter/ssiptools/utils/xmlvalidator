# -*- coding: utf-8 -*-
#    xmlvalidator reads and validates XML files against XML schema.
#    Copyright (C) 2019  Mark D. Driver
#
#    xmlvalidator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Test case and suite for XML validation methods.

@author: mark
"""

import logging
import sys
import pathlib
import unittest
from lxml import etree
import xmlvalidator.xmlvalidation as xmlvalidation

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class XMLValidationTestCase(unittest.TestCase):
    """Test case for validating the XML."""

    def setUp(self):
        """Test set up."""
        self.maxDiff = None
        self.xml_filename = (
            (pathlib.Path(__file__).parents[0] / "resources" / "ethanolnamespaced.cml")
            .absolute()
            .as_uri()
        )
        LOGGER.debug("XML file: %s", self.xml_filename)
        self.expected_contents = """<cml:molecule xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema" cml:id="LFQSCWFLJHTTHZ-UHFFFAOYSA-N" ssip:stdInChIKey="LFQSCWFLJHTTHZ-UHFFFAOYSA-N">
  <cml:atomArray>
    <cml:atom cml:id="a1" cml:elementType="C" cml:x3="-0.888300" cml:y3="0.167000" cml:z3="-0.027300"/>
    <cml:atom cml:id="a2" cml:elementType="C" cml:x3="0.465800" cml:y3="-0.511600" cml:z3="-0.036800"/>
    <cml:atom cml:id="a3" cml:elementType="O" cml:x3="1.431100" cml:y3="0.322900" cml:z3="0.586700"/>
    <cml:atom cml:id="a4" cml:elementType="H" cml:x3="-0.848700" cml:y3="1.117500" cml:z3="-0.569500"/>
    <cml:atom cml:id="a5" cml:elementType="H" cml:x3="-1.647100" cml:y3="-0.470400" cml:z3="-0.489600"/>
    <cml:atom cml:id="a6" cml:elementType="H" cml:x3="-1.196400" cml:y3="0.397800" cml:z3="0.997700"/>
    <cml:atom cml:id="a7" cml:elementType="H" cml:x3="0.792000" cml:y3="-0.722400" cml:z3="-1.059700"/>
    <cml:atom cml:id="a8" cml:elementType="H" cml:x3="0.424600" cml:y3="-1.455900" cml:z3="0.513800"/>
    <cml:atom cml:id="a9" cml:elementType="H" cml:x3="1.467100" cml:y3="1.155000" cml:z3="0.084800"/>
  </cml:atomArray>
  <cml:bondArray>
    <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
    <cml:bond cml:atomRefs2="a2 a3" cml:order="1"/>
    <cml:bond cml:atomRefs2="a1 a4" cml:order="1"/>
    <cml:bond cml:atomRefs2="a1 a5" cml:order="1"/>
    <cml:bond cml:atomRefs2="a1 a6" cml:order="1"/>
    <cml:bond cml:atomRefs2="a2 a7" cml:order="1"/>
    <cml:bond cml:atomRefs2="a2 a8" cml:order="1"/>
    <cml:bond cml:atomRefs2="a3 a9" cml:order="1"/>
  </cml:bondArray>
</cml:molecule>"""
        self.xml_etree = etree.XML(self.expected_contents)
        self.schema = xmlvalidation.create_xml_schema(xmlvalidation.CML_SCHEMA_PATH)

    def tearDown(self):
        """Clean up after tests."""
        del self.xml_filename
        del self.expected_contents
        del self.xml_etree
        del self.schema

    def test_validate_and_read_xml_file(self):
        """Test to see if file is read and validated."""
        actual_file_tree = xmlvalidation.validate_and_read_xml_file(
            self.xml_filename, xmlvalidation.CML_SCHEMA
        )
        self.assertMultiLineEqual(
            self.expected_contents, etree.tounicode(actual_file_tree)
        )

    def test_validate_xml(self):
        """Test to see if file is valid."""
        valid = xmlvalidation.validate_xml(self.xml_etree, xmlvalidation.CML_SCHEMA)
        self.assertEqual(None, valid)

    def test_validate_xml_file(self):
        """Test to see if etree is valid."""
        valid = xmlvalidation.validate_xml_file(self.xml_etree, self.schema)
        self.assertEqual(None, valid)

    def test_create_xml_schema(self):
        """Test to see if schema is created."""
        self.assertTrue(self.schema is not None)

    def test_read_xml_file(self):
        """Test to see if file is read to etree as expected."""
        actual_file_tree = xmlvalidation.read_xml_file(self.xml_filename)
        self.assertMultiLineEqual(
            self.expected_contents, etree.tounicode(actual_file_tree)
        )


def create_test_suite():
    """Create test suite with all tests from the different test case."""
    LOGGER.info("setting up loader and test suite")
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    LOGGER.debug("Adding %s", XMLValidationTestCase)
    suite.addTests(loader.loadTestsFromTestCase(XMLValidationTestCase))
    return suite


def run_tests():
    """Run test suite. Exits if there is a failure.

    Returns
    -------
    None
    """
    LOGGER.info("calling test suite method")
    suite = create_test_suite()
    LOGGER.info("running test suite")
    ret = (
        not unittest.TextTestRunner(verbosity=2, stream=sys.stderr)
        .run(suite)
        .wasSuccessful()
    )
    if ret:
        sys.exit(ret)


if __name__ == "__main__":
    run_tests()
