# XMLValidator #

XMLValidator is a small python module for reading and validating XML files against an XML Schema.
Schema instances for schemas created as part of work in the Hunter group are included for ease of use.

## Getting started ##

The latest release is available on [conda-forge](https://anaconda.org/conda-forge/xmlvalidator) conda-forge 
and can be installed with [conda](https://docs.conda.io/en/latest/) via the command:

    conda install -c conda-forge xmlvalidator
	
### Expected usage ###

To test that it has been installed try importing xmlvalidator and reading the example file.

    $ python
    Python 3.6.13 | packaged by conda-forge | (default, Feb 19 2021, 05:36:01) 
    [GCC 9.3.0] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> import xmlvalidator
    >>> xmlvalidator.validate_and_read_xml_file(xmlvalidator.CML_ETHANOL_EXAMPLE, xmlvalidator.CML_SCHEMA)
    <lxml.etree._ElementTree object at 0x7f2fe2e225c8>
    >>> 


### Contribution guidelines ###

This code has been released under the AGPLv3 license.
If you find any bugs please file an issue ticket.
Submission of pull requests for open issues or improvements are welcomed.

### Who do I talk to? ###

Any queries please contact Mark Driver.

### License

&copy; Mark Driver,  Christopher Hunter, Teodor Nikolov at the University of Cambridge

This is released under an AGPLv3 license for academic use.
Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:

 

Cambridge Enterprise Ltd
University of Cambridge
Hauser Forum
3 Charles Babbage Rd
Cambridge CB3 0GT
United Kingdom
Tel: +44 (0)1223 760339
Email: software@enterprise.cam.ac.uk
