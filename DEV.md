## Getting started from the source ##

To clone this repository from the following sources use the below commands.
	
*From University of Cambridge gitlab (using ssh):*

	git clone git@gitlab.developers.cam.ac.uk:ch/hunter/ssiptools/utils/xmlvalidator.git
	cd xmlvalidator

### Setting up the python environment ###

Details of an anaconda environment is provided in the repository with the required dependencies for this package.

	conda create -y -n xmlval

This creates an environment called 'xmlval' which can be loaded using:

	conda activate xmlval

Install the needed deps
       
	conda install -y -c conda-forge pip lxml pytest

### Using pip to install module ###

To install in your environment using pip run:

	pip install .

This installs it in your current python environment.


To run the associated unit tests:

	pytest xmlvalidator

### Documentation ###

Documentation can be rendered using sphinx.

	conda install -c conda-forge sphinx recommonmark
	cd docs
	make html

The rendered HTML can then be found in the build sub folder.

In progress: display of this documentation in a project wiki.

